#include "lunarium.h"
#include "ui_lunarium.h"
#include <iostream>
#include <QMessageBox>

Lunarium::Lunarium(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Lunarium)
{
    ui->setupUi(this);
}

Lunarium::~Lunarium()
{
    delete ui;
}

void Lunarium::on_VanillaGnome_clicked()
{
    system("sudo apt-add-repository -y ppa:gnome3-team/gnome3-staging && sudo apt upgrade && sudo apt dist-upgrade && sudo apt remove --purge -y gnome && sudo apt clean && sudo apt autoremove -y && sudo apt install gnome-core -y && sudo apt install -y --reinstall gnome-session gnome-session-wayland && sudo apt install -y gnome-tweak-tool && sudo apt install -y chrome-gnome-shell");
    QMessageBox::about(this, "Лог", "1).Были обновлены система и gnome\n 2).Был удалён лишний мусор из гнома благодаря его удалению и установке Gnome-core\n 3). Возвраащена классическая сессия \n 4).Установлена твик-уттилита и плагин для хрома чтобы ставить дополнения с extentions.gnome.org");
    ui->VanillaGnome->setText("Сделано");
}

void Lunarium::on_PLANK_clicked()
{
    system("sudo apt install plank");
    QMessageBox::about(this, "Лог", "1). Был установлен альтернативный DOCK(PLANK)");
    ui->PLANK->setText("Сделано");
}

void Lunarium::on_Numix_clicked()
{
    system("sudo add-apt-repository -y ppa:numix/ppa && sudo apt install -y numix-gtk-theme numix-icon-theme-circle");
    QMessageBox::about(this, "Лог", "1). Был добавлен репозиторий Numix\n 2). Установлена gtk тема Numix и иконки");
    ui->Numix->setText("Сделано");
}

void Lunarium::on_bitLibs_clicked()
{
    system("sudo dpkg --add-architecture i386 && sudo apt install -y libc6:i386 libasound2:i386 libasound2-data:i386 libasound2-plugins:i386 libgtk2.0-0:i386 libsdl2-2.0-0:i386 libsdl2-image-2.0-0:i386 libfreetype6:i386 libcurl3:i386");
    ui->bitLibs->setText("Сделано");
}

void Lunarium::on_gcc_clicked()
{
    system("sudo add-apt-repository ppa:ubuntu-toolchain-r/test && sudo apt install gcc-8 g++-8 gcc g++");
    ui->gcc->setText("Сделано");
}

void Lunarium::on_archive_clicked()
{
    system("sudo apt install -y p7zip-rar rar unrar unace arj cabextract");
    ui->archive->setText("Сделано");
}

void Lunarium::on_gayvidia_clicked()
{
    system("sudo add-apt-repository -y ppa:graphics-drivers/ppa && sudo apt install -y -f nvidia-396*");
    QMessageBox::about(this, "Лог", "1). Добавлен репозиторий для Nvidia\n2). Установлены драйверы nvidia");
    ui->gayvidia->setText("Сделано");
}

void Lunarium::on_AMDPO_clicked()
{
    system("sudo apt install libvulkan1 libvulkan1:i386 libvulkan-dev libvulkan-dev:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386 vulkan-utils");
    ui->AMDPO->setText("Сделано");
}

void Lunarium::on_Intel_clicked()
{
    system("sudo apt install --reinstall intel-microcode");
    ui->Intel->setText("Сделано");
}

void Lunarium::on_AMD_clicked()
{
    system("sudo apt install --reinstall amd64-microcode");
    ui->AMD->setText("Сделано");
}
