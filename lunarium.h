#ifndef LUNARIUM_H
#define LUNARIUM_H

#include <QMainWindow>

namespace Ui {
class Lunarium;
}

class Lunarium : public QMainWindow
{
    Q_OBJECT

public:
    explicit Lunarium(QWidget *parent = nullptr);
    ~Lunarium();

private slots:
    void on_VanillaGnome_clicked();

    void on_PLANK_clicked();

    void on_Numix_clicked();

    void on_bitLibs_clicked();

    void on_gcc_clicked();

    void on_archive_clicked();

    void on_gayvidia_clicked();

    void on_AMDPO_clicked();

    void on_Intel_clicked();

    void on_AMD_clicked();

private:
    Ui::Lunarium *ui;
};

#endif // LUNARIUM_H
